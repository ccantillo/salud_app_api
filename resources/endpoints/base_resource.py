from quart_openapi import Resource
from resources import utils


class BaseResource(Resource):
    user_details = {}
    return_status_code = 401
    account_id = None
    account_obj = None

    def __init__(self, *args, **kwargs):
        pass

    async def do_auth(self, request, **kwargs):
        # print "auth type %s" % self.auth_type
        if self.auth_type == 'api':
            return self.do_auth_api(request)
        if self.auth_type == 'jwt':
            # print kwargs
            if not kwargs.get('account_id'):
                return False
            return await self.do_auth_jwt(request,
                                          account_id=kwargs.get('account_id'))

    async def do_auth_api(self, request):
        """Auth API key"""
        if not utils._auth(request):
            self.return_status_code = 401
            self.user_details = {}
            return False
        self.return_status_code = 200
        self.user_details = {}
        return True

    async def do_auth_jwt(self, request, account_id=None):
        """Auth with jwt for portal and other frontends"""
        # get the auth api id of the contact / get account
        if not account_id:
            # print "No account ID"
            return False
        jj = utils.jwt_auth(request, get_full=True)
        print(jj)
        if not jj:
            self.return_status_code = 401
            self.user_details = {}
            return False
        # Need to check if they are allowed to access this Billing Account ID
        # By doing a revers lookup in the Billing DB and getting the CRM Account Number
        # Then we cross reference this CRM Account Number with the Billing Account Object
        # If it matches, then we know this JWT has access to this account.
        crm_account_number = await self.get_crm_account_num_by_billing_account_id(account_id)
        try:
            if crm_account_number in jj.get('acl'):
                if 'admin' in jj['acl'][crm_account_number].get('roles'):
                    return True
                else:
                    if 'billing' in jj['acl'][crm_account_number].get('roles'):
                        return True
        except Exception as e:
            print("Exception: {0}".format(str(e)))
            print("Has no ACL")
            return False

        return True



