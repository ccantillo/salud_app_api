import hashlib
import json

from marshmallow import ValidationError
from quart import request
from quart_openapi import Resource

from resources.endpoints.base_resource import BaseResource
from resources.models.users import Users
from resources.schemas.auth import AdminUserAuthSchema
from resources.schemas.users import UsersSchema
from resources.utils import gen_response, generate_jwt_token_admin, app_admin_auth


class AdminLoginResource(BaseResource, Resource):

    async def post(self):

        payload = await request.get_data()
        json_payload = json.loads(payload)

        print(f"the payload is {json_payload}")
        # Validate Json:
        try:
            validation_result = AdminUserAuthSchema(unknown='EXCLUDE').load(json_payload)
        except ValidationError as err:
            err.field = 'errors'
            valid_data = err.valid_data
            return await gen_response(valid_data, True, err.messages, portal_response=True), 400

        try:
            user = await Users.get(username=json_payload.get('username'))
        except Exception as e:
            return await gen_response(None, True, 'Invalid credentials', portal_response=False), 401

        password_hash = hashlib.pbkdf2_hmac(
            'sha256',  # The hash digest algorithm for HMAC
            json_payload.get('password').encode('utf-8'),  # Convert the password to bytes
            user.password_salt,  # Provide the salt
            100000,  # It is recommended to use at least 100,000 iterations of SHA-256
            dklen=128  # Get a 128 byte key
        )

        if user.password == password_hash:
            # Password is correct, generate token.
            response_model = UsersSchema(exclude=['password', 'password_salt']).dump(user)
            response_model['access_token'] = generate_jwt_token_admin(user)
            response_model['refresh_token'] = generate_jwt_token_admin(user)

            return await gen_response(response_model, False, 'Logged in', portal_response=False), 200
        else:
            return await gen_response(None, True, 'Invalid credentials', portal_response=False), 401


class AdminRefreshResource(Resource):

    async def get(self, **kwargs):
        token_payload = kwargs.get('token_payload')
        user_id = token_payload.get('id')
        user = await Users.get(pk=user_id)
        response_model = UsersSchema(exclude=['password', 'password_salt']).dump(user)
        response_model['access_token'] = generate_jwt_token_admin(user)
        response_model['refresh_token'] = generate_jwt_token_admin(user)

        return await gen_response(response_model, False, 'Token Refreshed', portal_response=False), 200