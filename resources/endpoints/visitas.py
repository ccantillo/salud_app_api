import json
from marshmallow import ValidationError
from resources.endpoints.base_resource import BaseResource
from quart_openapi import Resource

from resources.models.visitas import Visitas
from resources.schemas.appointments import AppointmentsSchema
from resources.schemas.doctor import DoctorSchema
from resources.schemas.pacient import PacientSchema
from resources.schemas.visitas import VisitasSchema
from resources.utils import gen_response, generate_password_hash, app_admin_auth
from quart import request


class AppVisitasResource(BaseResource, Resource):
    async def get(self, visita_id, **kwargs):
        obj = Visitas()
        schema = VisitasSchema()
        response_obj = await obj.get(pk=visita_id).prefetch_related("doctor").prefetch_related("appointment__pacient")
        response_model = schema.dump(response_obj)
        response_model["doctor"] = DoctorSchema(exclude=['password', 'password_salt']).dump(response_obj.doctor)
        response_model["appointment"] = AppointmentsSchema().dump(response_obj.appointment)
        response_model["pacient"] = PacientSchema().dump(response_obj.appointment.pacient)

        return await gen_response(response_model, False, 'Viewing doctor', portal_response=False), 200


    async def patch(self, visita_id, **kwargs):

        obj = Visitas()
        schema = VisitasSchema()
        response_obj = await obj.get(pk=visita_id)

        payload = await request.get_data()
        json_payload = json.loads(payload)
        if json_payload.get('password'):
            # Create password hash before saving
            pw_hash = await generate_password_hash(json_payload.get('password'))
            pw_hash_key = pw_hash.get('key')
            json_payload['password'] = pw_hash.get('key')
            pw_hash_key2 = pw_hash.get('key')
            json_payload['password_salt'] = pw_hash.get('salt')
        for k, v in json_payload.items():
            setattr(response_obj, k, v)
        # Validate Json:
        try:
            user_dict = schema.dump(response_obj)
            # validation_result = UsersSchema(unknown='EXCLUDE').load(user_dict)
        except ValidationError as err:
            err.field = 'errors'
            valid_data = err.valid_data
            return await gen_response(valid_data, True, err.messages, portal_response=False), 400

        await response_obj.save()
        response_model = schema.dump(response_obj)
        return await gen_response(response_model, False, 'Viewing doctor', portal_response=True), 200

    async def delete(self, visita_id, **kwargs):

        obj = Visitas()
        schema = VisitasSchema()
        response_obj = await obj.get(pk=visita_id)
        response_model = schema.dump(response_obj)
        await response_obj.delete()
        # return response_model.get('product_image')

        return await gen_response(response_model, False, 'Product deleted', portal_response=False), 200


class AppVisitasResourceList(BaseResource, Resource):
    async def get(self, **kwargs):
        token_payload = kwargs.get('token_payload')
        doctor_id = token_payload.get('id')
        obj = Visitas()
        schema = VisitasSchema()
        filters = {
            "username__icontains": request.args.get('search'),  # backwards compatibility
            "first_name__icontains": request.args.get('first_name'),
            "last_name__icontains": request.args.get('last_name'),
            "username__icontains": request.args.get('email'),
            "doctor_id": int(doctor_id)
        }
        filters = {key: value for key, value in filters.items() if value is not None and value != 'null'}
        print(f"the filter used are {filters}")
        obj_list = await obj.filter(**filters).prefetch_related("doctor", 'appointment__pacient')
        response_list = []
        for response_obj in obj_list:
            response_model = schema.dump(response_obj)
            response_model["doctor"] = DoctorSchema(exclude=['password', 'password_salt']).dump(response_obj.doctor)
            response_model["appointment"] = AppointmentsSchema().dump(response_obj.appointment)
            response_model["pacient"] = PacientSchema().dump(response_obj.appointment.pacient)

            response_list.append(response_model)

        response = {
            "model_list": response_list,
            "total_pages": 0,
            "page": 0
        }
        return await gen_response(response, False, 'Viewing Product List', portal_response=False), 200

    async def post(self, **kwargs):

        token_payload = kwargs.get('token_payload')
        doctor_id = token_payload.get('id')

        payload = await request.get_data()
        json_payload = json.loads(payload)
        obj = Visitas()
        schema = VisitasSchema(unknown='EXCLUDE')
        try:
            validation_result = schema.load(json_payload)
        except ValidationError as err:
            err.field = 'errors'
            valid_data = err.valid_data
            return await gen_response(valid_data, True, err.messages, portal_response=False), 400
        validation_result["doctor_id"] = int(doctor_id)
        print(f"the validation is {validation_result}")
        response_obj = await obj.create(**validation_result)
        response_model = schema.dump(response_obj)

        return await gen_response(response_model, False, 'user Created', portal_response=False), 200