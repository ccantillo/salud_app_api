from tortoise.models import Model
from tortoise import fields


class Cliente(Model):
    name = fields.CharField(max_length=100, blank=False, Null=False)
    nit = fields.CharField(max_length=100, blank=False, Null=False)
    contratista = fields.relational.ForeignKeyField('models.Contratista', on_delete=fields.CASCADE)
