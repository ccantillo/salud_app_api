
from resources.models.users import Users
from resources.models.visitas import Visitas

__models__ = [
    Users,
    Visitas,
]