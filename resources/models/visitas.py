from tortoise.models import Model
from tortoise import fields


class Visitas(Model):
    doctor = fields.relational.ForeignKeyField('models.Doctor', on_delete=fields.CASCADE)
    appointment = fields.relational.ForeignKeyField('models.Appointments', on_delete=fields.CASCADE)
    completed = fields.BooleanField(default=False)
