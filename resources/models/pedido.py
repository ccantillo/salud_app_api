from tortoise.models import Model
from tortoise import fields


class Pedido(Model):
    name = fields.CharField(max_length=100, blank=False, Null=False)
    nit = fields.CharField(max_length=100, blank=False, Null=False)
    contratista = fields.relational.ForeignKeyField('models.Contratista', on_delete=fields.CASCADE)
    descripcion = fields.TextField()
    total = fields.IntField(null=False, default=0)
    fecha = fields.data.DateField(auto_now_add=True)
    datetime = fields.data.DatetimeField(auto_now_add=True)
    cliente = fields.relational.ForeignKeyField('models.Cliente', on_delete=fields.SET_NULL)
    productos = fields.ManyToManyField('models.Producto', null=True, blank=True)