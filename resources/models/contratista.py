from tortoise.models import Model
from tortoise import fields


class Contratista(Model):
    name = fields.CharField(max_length=100, blank=False, Null=False)
    nit = fields.CharField(max_length=100, blank=False, Null=False)