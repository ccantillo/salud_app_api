from tortoise.models import Model
from tortoise import fields


class Users(Model):

    username = fields.CharField(max_length=100, blank=False, Null=False)
    first_name = fields.CharField(max_length=50, blank=True, null=True)
    last_name = fields.CharField(max_length=50, blank=True, null=True)
    password = fields.BinaryField(blank=True, null=True)
    password_salt = fields.BinaryField(blank=True, null=True)
    super_admin = fields.BooleanField(default=False)

    @classmethod
    async def add_user(cls, **kwargs):
        user = await Users.create(**kwargs)
        return user