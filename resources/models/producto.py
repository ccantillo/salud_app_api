from tortoise.models import Model
from tortoise import fields


class Producto(Model):
    name = fields.CharField(max_length=100, blank=False, Null=False)
    precio = fields.IntField(null=False, default=0)
    descripcion = fields.TextField(null=True)
    disponible = fields.BooleanField(default=True)