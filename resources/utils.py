import email
from datetime import date
import hashlib
import os
import uuid
from datetime import timedelta, datetime
from email.mime.application import MIMEApplication

import jwt
import smtplib
from quart import jsonify, request
from collections import MutableMapping
from contextlib import suppress
from functools import wraps
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

JWT_KEY = 'rRregrgERg54g564heRGRfdsgerhTREGRESDFwef3452fwefer'

def jwt_auth(request, get_full=False, testing=False):
    """JWT Authentication"""
    validated = False
    token = request.headers.get("Authorization")
    if not token:
        return None
    token = str(token).replace('Bearer ', '').replace('bearer ', '')
    try:
        j = jwt.decode(token, JWT_KEY, algorithms=['HS256'], verify=True)
        validated = True
    except Exception as e:
        return None
    if testing:
        if validated:
            j['validated'] = validated
        return j
    if j:
        user_id = j.get('id')
        if get_full:
            return j
        return user_id
    return None


def app_admin_auth(f):
    @wraps(f)
    async def decorated_function(*args, **kwargs):
        print('im here')
        token_payload = jwt_get_payload(request)

        if token_payload:
            # Token was valid, proceed to verify:
            kwargs['token_payload'] = token_payload
            return await f(*args, **kwargs)
        else:
            # Return standard 401
            return jsonify({'result': 'Unauthorized'}), 401
    return decorated_function


def app_doctor_auth(f):
    @wraps(f)
    async def decorated_function(*args, **kwargs):
        token_payload = jwt_get_payload(request)

        print(f'the payload is {token_payload}')

        if token_payload:
            # Token was valid, proceed to verify:
            kwargs['token_payload'] = token_payload
            return await f(*args, **kwargs)
        else:
            # Return standard 401
            return jsonify({'result': 'Unauthorized'}), 401
    return decorated_function


def jwt_get_payload(request):
    # Decode jwt and return payload
    validated = False
    token = request.headers.get("Authorization")
    print(f"el token es {token}")
    if not token:
        return None
    token = str(token).replace('Bearer ', '').replace('bearer ', '')
    try:
        j = jwt.decode(token, JWT_KEY, algorithms=['HS256'], verify=True)
    except Exception as e:
        print("Exception in jwt_auth: {0}".format(e))
        return None

    if j:
        return j
    return None


def generate_jwt_token_admin(user, expsec=None):

    if not expsec:
        expsec = 3600

    exp = datetime.utcnow() + timedelta(seconds=int(expsec))
    payload = {
        'exp': exp,
        'id': str(user.pk),
        'iat': datetime.utcnow(),
    }
    encoded = jwt.encode(
            payload,
            JWT_KEY,
            algorithm='HS256'
    )
    # decode converts byte string to string, use encode for the reverse:
    return encoded.decode('UTF-8')


async def gen_response(result, errors, message, portal_response,page_size=None, **kwargs):
    d = {}
    d['result'] = result
    d['errors'] = errors
    d['message'] = message
    d['page_size'] = page_size
    for k, v in kwargs.items():
        d[k] = v
    if portal_response:

        cleaned_output = portal_clean_json_output(d)
        return jsonify(cleaned_output)

    return jsonify(d)


def portal_clean_json_output(json_input):

    # Remove any fields that should not be displayed in /portal endpoints:

    remove_fields = [
        'max_users',
        'notifications__low_balance__sent_low_balance',
        'created',
        'billing_mode',
        'is_reseller',
        'superduper_admin',
        'wm_allow_additions',
        'reseller_id',
        'revision',
        'node',
        'request_id',
        'status',
        'auth_token',
        'ui_metadata'
    ]

    for field in list(json_input):
        if field in remove_fields:
            with suppress(KeyError):
                del json_input[field]
    for value in json_input.values():
        if isinstance(value, MutableMapping):
            portal_clean_json_output(value)

    return json_input

def generate_jwt_token_app_ui(user, expsec=None):

    if not expsec:
        expsec = 7200

    exp = datetime.utcnow() + timedelta(seconds=int(expsec))
    payload = {
        'exp': exp,
        'id': str(user.pk),
        'iat': datetime.utcnow(),
    }
    encoded = jwt.encode(
            payload,
            JWT_KEY,
            algorithm='HS256'
    )
    # decode converts byte string to string, use encode for the reverse:
    return encoded.decode('UTF-8')


async def generate_password_hash(password):
    # generate password hash and return encrypted string
    salt = os.urandom(32)

    password_hash = hashlib.pbkdf2_hmac(
        'sha256',  # The hash digest algorithm for HMAC
        password.encode('utf-8'),  # Convert the password to bytes
        salt,  # Provide the salt
        100000,  # It is recommended to use at least 100,000 iterations of SHA-256
        dklen=128  # Get a 128 byte key
    )

    pw_hash = {'key': password_hash, 'salt': salt}

    return pw_hash


'''def send_email(recipients, subject, body, attachment=None, attachment_name=None):
    # Generic send email utility coming from ITP VOICE
    port = cfg.get('smtp_port')
    password = cfg.get('smtp_password')
    smtp_host = cfg.get('smtp_host')
    smtp_username = cfg.get('smtp_username')

    # Create a text/plain message

    msg = email.mime.multipart.MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = 'no-reply@itp247.com'

    # The main body is just another attachment
    body = email.mime.text.MIMEText(body, 'html')
    msg.attach(body)

    if attachment:
        # PDF attachment
        att = email.mime.application.MIMEApplication(attachment, _subtype="pdf")
        att.add_header('Content-Disposition', 'attachment', filename=attachment_name)
        msg.attach(att)

    with smtplib.SMTP_SSL(smtp_host, port) as server:
        server.login(smtp_username, password)
        for recipient in recipients:
            server.sendmail(smtp_username, recipient, msg.as_string())'''


