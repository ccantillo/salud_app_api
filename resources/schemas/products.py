import base64
import io

from marshmallow import Schema, fields, ValidationError, pre_dump, post_dump
from marshmallow.validate import OneOf
from PIL import Image


class ParentProduct(fields.Field):
    """Field that serializes tortoise orm type objects to None fields
    """
    def _serialize(self, value, attr, obj, **kwargs):
        if not isinstance(value, dict):
            return ""
        return value


class ProductManufacturerSchema(Schema):
    pk = fields.Int()
    name = fields.Str()
    description = fields.Str()
    website = fields.Str()


class ProductKeyNamesSchema(Schema):
    pk = fields.Int()
    name = fields.Str()


'''class ItemType(fields.Field):
    def _serialize(self, value, attr, obj, **kwargs):
        #return obj.get('product_key')
        return obj.item_type


    def _deserialize(self, value, attr, data, **kwargs):
        return obj.item_type'''

class ProductImage(fields.Field):
    """
    """
    def _serialize(self, value, attr, obj, **kwargs):
        if value is not None:
            file = Image.open(io.BytesIO(value))
            file_type = file.format
            file_type = file_type.lower()
            mime_string = f'data:image/{file_type};base64,'
            base64_image_bytes = base64.b64encode(value)
            base64_image_string = base64_image_bytes.decode()
            final_base64_string = f'{mime_string}{base64_image_string}'
            return final_base64_string
        else:
            return str("")


class ProductsSchema(Schema):
    pk = fields.Int()
    name = fields.Str()
    category = fields.Str(validate=OneOf(['medicine', 'equipment']), default="medicine")
    currency = fields.Str(validate=OneOf(['USD', 'CAD']))
    cost = fields.Float()
    price = fields.Float()
    description = fields.Str()
    disabled = fields.Boolean()
    metadata = fields.Dict()
    auto_provision = fields.Boolean()
    depends_on_service_address = fields.Boolean()
    item_type = fields.Method('item_type_copy')    # fields.Str(allow_none=True, dump_only=True)
    product_image = ProductImage(required=False)

    def item_type_copy(self, obj):
        try:
            if obj.product_key:
                return obj.product_key
        except Exception as e:
            return None



