from marshmallow import Schema, fields


class HistorySchema(Schema):
    pk = fields.Int()
    visita_id = fields.Int()
    motives = fields.Str()
    sickness_description = fields.Str()