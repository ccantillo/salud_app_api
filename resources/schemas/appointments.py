from marshmallow import Schema, fields


class AppointmentsSchema(Schema):
    pk = fields.Int()
    pacient_id = fields.Int()
    date_created = fields.Str()