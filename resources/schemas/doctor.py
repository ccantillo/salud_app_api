from marshmallow import Schema, fields


class DoctorSchema(Schema):
    pk = fields.Int()
    username = fields.Str()
    first_name = fields.Str()
    last_name = fields.Str()
    password = fields.Str()
    password_salt = fields.Str()