import base64
import io

from marshmallow import Schema, fields, ValidationError, pre_dump, post_dump
from marshmallow.validate import OneOf
from PIL import Image


class PacientImage(fields.Field):

    def _serialize(self, value, attr, obj, **kwargs):
        if value is not None:
            file = Image.open(io.BytesIO(value))
            file_type = file.format
            file_type = file_type.lower()
            mime_string = f'data:image/{file_type};base64,'
            base64_image_bytes = base64.b64encode(value)
            base64_image_string = base64_image_bytes.decode()
            final_base64_string = f'{mime_string}{base64_image_string}'
            return final_base64_string
        else:
            return str("")


class PacientSchema(Schema):
    pk = fields.Int()
    firstname = fields.Str()
    lastname = fields.Str()
    title = fields.Str()
    identification = fields.Str()
    email = fields.Str()
    phone = fields.Str()
    mobile = fields.Str()
    pacient_image = PacientImage(required=False, dump_only=True)
    priority = fields.Int()