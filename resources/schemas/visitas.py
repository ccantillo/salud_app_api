from marshmallow import Schema, fields


class VisitasSchema(Schema):
    pk = fields.Int()
    doctor_id = fields.Int()
    appointment_id = fields.Int()