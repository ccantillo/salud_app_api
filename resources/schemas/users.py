from marshmallow import Schema, fields


class UsersSchema(Schema):
    pk = fields.Int()
    first_name = fields.Str(required=True)
    last_name = fields.Str(required=True)
    username = fields.Email(required=True)
    super_admin = fields.Boolean()
    password = fields.Str()
    password_salt = fields.Str()