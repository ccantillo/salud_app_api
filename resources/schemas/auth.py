from marshmallow import Schema, fields


class AdminUserAuthSchema(Schema):
    username = fields.Email(required=True)
    password = fields.Str(required=True)
    access_token = fields.Str()
    refresh_token = fields.Str()