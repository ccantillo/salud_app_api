from quart_openapi import Pint
from tortoise.contrib.quart import register_tortoise

from quart_cors import cors, route_cors

# init the app
from resources.endpoints.admin_auth import AdminLoginResource
from resources.endpoints.visitas import AppVisitasResourceList, AppVisitasResource

app = Pint(__name__, title='app api')
app = cors(app, allow_origin="*")


# init Database connection

mysql_server_host = 'bqindpg3ljtoqclsosya-mysql.services.clever-cloud.com'
mysql_server_port = '3306'
mysql_server_password = 'TwLWZTtvfnQii42BwsIq'
mysql_server_username = 'ubeosdjdgasbtjfr'
mysql_db_name = 'bqindpg3ljtoqclsosya'

register_tortoise(
    app,
    db_url=f"mysql://{mysql_server_username}:"
           f"{mysql_server_password}@{mysql_server_host}"
           f":{mysql_server_port}/{mysql_db_name}",
    modules={"models": ["resources.models"]},
    generate_schemas=True,
)

app.add_url_rule('/app/visitas', view_func=AppVisitasResourceList.as_view('app_visitas_resource_list'))
app.add_url_rule('/app/visitas/<string:visita_id>', view_func=AppVisitasResource.as_view('app_visitas_resource'))
app.add_url_rule('/app/admin/auth/login', view_func=AdminLoginResource.as_view('admin_auth_resource'))



# End app Endpoints

if __name__ == '__main__':

    app.run()